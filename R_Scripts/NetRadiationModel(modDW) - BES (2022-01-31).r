###
### R script by Jason Leach, 2008 04 22
### adapted from FORTRAN code by RD Moore
### see Moore et al 2005 HP and
###   Leach and Moore 2010 HP for details
### Updated by Dave West for Middle Vernon Creek - Feb, 2019
### Updated by Dave West for Bessette Creek - Jan 31, 2022
### Project name: Above-stream net radiation model
###   
### Objective: Models Q* using GLA hemispherical
###   photos and local climate data
###
### Inputs: Gap Light Analyzer output and
###   meteorological data
###
### Outputs: Modelled net radiation
###
###

###
warning('read steps first')

#Steps

# 1- run eaatmfunc first to populate results
# 2 - run eaatmfunc function code again with forced input variables and without the function
# 3 - Run Qcalcs for all hemispherical photos with the loop 
# 4 - Fully averaged Kdir, Kdiff, and Ldown
# Make sure atmospheric pressure is in kPA!

###
# Prepare workspace ----
###

rm(list=ls())

### packages

library(hydroGOF)
library(dplyr)
library(zoo)
library(stringr)
library(xts)
library(dygraphs)
library(tidyverse)
library(lubridate)
library(readxl)

###
# Load data ----
###

### set working directory

setwd("C:/Users/dwest/Repositories/1446-daves_general_data_prep")


###
##
# Section 1 - Dygraph function ----
##
##



dyg <- function(datf,tt,X1=NULL,X2=NULL,X3=NULL){
  
  #  datf$ISO = datf$ISO + 8*60*60  # Not sure why this is getting shifted.. but this is a manual fix
  
  datxts <- xts(datf[,c(X1,X2,X3)], order.by= datf[,tt])
  
  tzone(datxts) <- "America/Los_Angeles"
  
  head(datxts)
  tail(datxts)
  
  per <- structure(
    list(difftime = structure(0, units = "secs", class = "difftime"),
         frequency = 0,
         start = start(datxts),
         end = end(datxts),
         units = "secs",
         scale = "seconds",
         label = "second"),
    class = "periodicity")
  
  
  dygraph(datxts,periodicity=per) %>% 
    dyOptions(connectSeparatedPoints = TRUE, sigFigs=3)
  
}





###



###
###  Import meteorology data ----
###

dat <- read.csv("Output/climate and rad.csv")
dat$ISO <- as.POSIXct(strptime(dat$ISO, "%Y-%m-%d %H:%M"))

str(dat)


# Check to ensure no NA's or missing data
x = numeric(0)

for (i in 1:length(dat)){
  x[i] = sum(is.na(dat[,i]))
}
x  # This should produce a string of zeros if no NA's (except cloud cover.. not used here anyway)

colnames(dat)

dat$X = NULL


###
#   Prefunction calcs (that aren't dependent on viewfactor or gap fraction)
###

###
#  User-defined parameter specifications ----
###

# Site Longitude (Deg + Minutes + Seconds)
Long = 118.958996      # updated for BES 
#122?32'10.86"

# Standard Longitude (120 for Pacific Time Zone)
StLong = 120


# Site Latitude (Deg + Minutes + Seconds)
lat = 50.245975  # updated for BES
#49?14'44.86"

# Albedo of water
a = 0.05

# emissivity of vegetation
ef = 0.97

# emissivity of water
ew = 0.95

###
#  Function to calculate atmospheric emissivity for a given air temp and solar rad ----
###

eaatmfunc <- function(kdir.in, Ta, glafile){

  # To populate variables for the Qcalcs function # Step 2
 kdir.in = dat$kdirfin; Ta = 'ta'; glafile = "Data/GLA Output .txt Files/DSCN7623.txt"     # Turn this on when running the code without function (Step 2)
  
# Load a sample GLA file (replace glafile below)
  
 
  
  dat.gf <- read.table(glafile,
                     skip = 3, header = T,
                    sep = ';', col.names=c('Alt', 'Azi', 'Altitude', 'Azimuth', 'SkySky', 'SkyTop', 'FolTop', 'FolSky', 'Total', 'Sky', 'Mask',  'Total.1', 'Gap.Can', 'Gap.Topo', 'Gap.Both', 'Canopy', 'Site', 'Id_UOC',  'Id_SOC',  'X'))
  
# Extract required outputs from sample GLA file
  
  #degree to radian conversion
  cr=pi/180
  
  alt.rad=dat.gf$Altitude*cr
  azi.rad=dat.gf$Azimuth*cr
  
  dazi=5.0*cr
  dalt=5.0*cr
  
  cos.alt=cos(alt.rad)
  sin.alt=sin(alt.rad)
  
# Set open Kdown flux density
OP.Kdown.fd <- dat$kdifffin+ kdir.in

###
# Atmospheric Pressure calculation 
###

#P is atm. pressure,
#P0 is mean atmospheric pressure at sea level
#P=101.3*((293-(0.0065)*620)/293)^(5.26) (Jason's)
P = dat$atmp   # MVC I think this can be a vector (???) it was an single point
P0=101.3 

###
### Part 2: - Determining solar path (zenith ----
###           and azimuth) for study period.
###         - Determining Direct and Diffuse
###           Kdown components using:
###	        - Open site Kdown (lumped Dir
###               + Dif) (Measured)
###		- Extraterrestrial Kdown
###               (above atmosphere)
###               (Calculated using Iqbal 1983))
###		- Direct and Diffuse Kdown
###               components using Erbs et al (1982)
###               empirical relationship
###


#ISO date and time
#hh = floor(dat$Time/100)
#mm = dat$Time - 100*hh
hh = as.numeric(strftime(dat$ISO, format="%H"))
mm = as.numeric(strftime(dat$ISO, format="%M"))

dat$Day <- as.numeric(strftime(dat$ISO, format="%j"))

#da=paste(dat$Year, dat$Day, hh, mm)
#tiso = strptime(da, format='%Y %j %H %M')
tiso = dat$ISO
df=data.frame(tiso,dat)

#calculating gamma; day angle
gamma = (2*pi*(dat$Day)-1)/365


latrad = (pi/180)*lat

#calculating solar declination using Iqbal, M. 1983. Introduction to Solar Radiation
#for each study day (in radians)

dec = (0.006918 - 0.399912*cos(gamma) + 0.070257*sin(gamma)
       - 0.006758*cos(2*gamma) + 0.000907*sin(2*gamma)
       - 0.002697*cos(3*gamma) + 0.00148*sin(3*gamma))


#calculating equation of time Et, again from Iqbal 1983

Et = (0.000075 + 0.001868*cos(gamma) - 0.032077*sin(gamma)
      - 0.014615*cos(2*gamma) - 0.04089*sin(2*gamma))*(229.18)/60  #229.18 converts to minutes, 60 converts to hours


t = hh + mm/60 
latime = t + (StLong - Long)/15 + Et


#calculating cosine solar zenith angle; in rads
#omega is the hour angle (noon = zero, morning(+))
omega = (12.0 - latime)*0.26179939

coszen = sin(dec)*sin(latrad) + cos(dec)*cos(latrad)*cos(omega)


#setting coszen = NA for when sun position is below the horizon
coszen[coszen < 0] = NA

solzen = acos(coszen)
sinzen = sin(solzen)

x = (coszen*sin(latrad) - sin(dec))/(sinzen*cos(latrad))

#degree to radian conversion
cr=pi/180

psi=acos(x)/cr #in degrees
psi = psi*(omega >= 0) + (0 - psi)*(omega < 0)

#Determining extraterrestrial solar
#radiation and Diffuse and Direct components
#of measured Kdown

#solar constant = 1367 Wm^-2
SolCon = 1367

#determining extraterrestrial solar radiation
Kpot = SolCon*coszen

#determining kt = ratio of global to extraterrestrial solar radiation
kt = OP.Kdown.fd/Kpot

#determining the kt and kd ratios using Erbs et al 1982
kd = (1.0086 - 0.178*kt) *(kt  <= 0.24) +
  (0.951 - 0.1604*kt + 4.388*kt^2 - 16.638*kt^3
   +  12.336*kt^4)*(kt > 0.22 & kt<=0.80) +
  0.197*(kt>0.80)

#determine the amount of Diffuse solar radiation
Kdiff = dat$kdifffin

#Determine Kdirect
Kdir = kdir.in

###
### Part 4: Calculating atmospheric emissivity ----
### using Prata (1996)
###

#Calculations for atmospheric emissivity
TaK=dat[,Ta] + 273.15
eps.s=0.611*exp((2.5*10^6/461)*((1/273)-(1/TaK)))
eps.a = (dat$rh/100)*eps.s

#ea -> Prata (1996) emissivity
w = 465*(eps.a/TaK) #46.5 (cmKhPa^-1) therefore 465 corrects for kPa
ea.Prata = 1-(1+w)*exp(-(1.2+3.0*w)^0.5)

ea.clear=ea.Prata

# plot(tiso[1000:1100],ea.clear[1000:1100], type='l')

#Calculating maximum incoming shortwave radiation (Kmax) under a cloud-free sky
#where SolCon = 1367 W/m^2, ea is clear sky atm. emissivity, P is atm. pressure,
#P0 is mean atmospheric pressure at sea level and coszen is the cosine of the
#solar zenith angle  from Brock and Arnold (2000)

psia = 0.75 #clear sky transmissivity
Kmax=SolCon*(psia^(P/P0))*coszen
Kmax[is.na(Kmax)] = 0


#cloud correction function as specified by Arnold et al. (1996)
#kappa is mean value for various cloud types (from Braithwaite and Olesen 1990)
n.rat=(OP.Kdown.fd/Kmax)

#plot(OP.Kdown.fd[10800:11000],type='l', main='Measured Kdown and Kmax', ylab='Kdir')
#lines(Kmax[10800:11000], col='green')
#legend("topright",col=c("black","green"),lty=1,legend=c("Kdown","Kmax"),cex=0.6,bg='white')

#plot(tiso[10000:10300],n.rat[10000:10300],type='l', xaxt='n')
#r <- as.POSIXct(round(range(tiso), "hours"))
#axis.POSIXct(1, at = seq(r[1], r[2], by = "hour"), format = "%H")


n=(1-n.rat)*(n.rat > 0.2) + (1)*(n.rat <= 0.2) 

kappa=0.26 

ea=(1+kappa*n)*ea.clear




#correct -Inf ea values to NA
ea[ea==-Inf]=NA
ea[ea==Inf]=NA 
ea[ea > 1]= NA#
ea[ea < 0.6] = NA


#The following produces night-time ea values using the mean values of
#the day before and the day after. 

hour= hh
ea.df=data.frame(tiso, dat$Day, hour, ea)

d1 = ea.df$dat.Day[1]
dn = ea.df$dat.Day[length(ea.df$dat.Day)]
ea.n = NULL
ea.df2 <- ea.df  



ea.df2$ea <- na.approx(ea.df2$ea,rule=2)



#for(i in d1:dn){
#    ea.x = subset(ea.df, dat.Day >= i & dat.Day <= i+1, select=c(tiso, ea, dat.Day, hour))
#    ea.n = mean(ea.x$ea, na.rm=T)
#    ea.df2[ea.df2$dat.Day == i & ea.df2$hour > 12 & is.na(ea.df2$ea) | ea.df2$dat.Day == i+1 & ea.df2$hour < 12 & is.na(ea.df2$ea),'ea'] <- ea.n
#   ea.df2[which(ea.df2$dat.Day == i & ea.df2$hour > 12 & ea.df2$solzen.deg > 70 | ea.df2$dat.Day == i+1 & ea.df2$hour < 12 & ea.df2$solzen.deg > 70),'ea'] <- ea.n

#}


return(ea.df2$ea)

}

#
# Create atmospheric emissivities to use for Ldown for CC scenarios ----
#

dat$ea.atm = eaatmfunc(kdir.in = dat$kdirfin, Ta = 'ta', glafile = "Data/GLA Output .txt Files/DSCN7623.txt")
dat$ea.atm.m45 = eaatmfunc(kdir.in = dat$kdirfin, Ta = 'air_temperature_2085_canesm45', glafile = "Data/GLA Output .txt Files/DSCN7623.txt")
dat$ea.atm.m85 = eaatmfunc(kdir.in = dat$kdirfin, Ta = 'air_temperature_2085_canesm85', glafile = "Data/GLA Output .txt Files/DSCN7623.txt")


dyg(dat, "ISO", "kdirfin")
dyg(dat, "ISO", "kdifffin")
dyg(dat, "ISO", "atmp")
dyg(dat, "ISO", "rh")
dyg(dat, "ISO", "ta")
dyg(dat, "ISO", "ea.atm")


str(dat)

# Export in case don't want to run this all again

write.csv(dat,"C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/atm_emissivity_dat.csv")

###
###Running a loop to run all photos ----
###

dat <- read.csv("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/atm_emissivity_dat.csv")
dat$ISO <- as.POSIXct(strftime(dat$ISO, "%Y-%m-%d %H:%M", tz = "America/Los_Angeles"))


Qcalcs <- function(h, kdir.in, Ta, eain){
  
  assign('dat.gf',read.table(h,
  skip = 3, header = T,
  sep = ';', col.names=c('Alt', 'Azi', 'Altitude', 'Azimuth', 'SkySky', 'SkyTop', 'FolTop', 'FolSky', 'Total', 'Sky', 'Mask',  'Total.1', 'Gap.Can', 'Gap.Topo', 'Gap.Both', 'Canopy', 'Site', 'Id_UOC',  'Id_SOC',  'X')))


###
# Run this to avoid the function (Optional) ----
###

  #kdir.in <- dat$Kdir.cc; Ta = 'Ta.catch'
  #h = "Data/GLA Output .txt Files/DSCN7623.txt"
  
  #dat.gf <- read.table("wpt106.txt",
  #                    skip = 3, header = T,
  #                   sep = ';', col.names=c('Alt', 'Azi', 'Altitude', 'Azimuth', 'SkySky', 'SkyTop', 'FolTop', 'FolSky', 'Total', 'Sky', 'Mask',  'Total.1', 'Gap.Can', 'Gap.Topo', 'Gap.Both', 'Canopy', 'Site', 'Id_UOC',  'Id_SOC',  'X'))


##
### Part 1: Calculate sky view factor (vf) ----
### using GLA output
###


#degree to radian conversion
cr=pi/180

alt.rad=dat.gf$Altitude*cr
azi.rad=dat.gf$Azimuth*cr

dazi=5.0*cr
dalt=5.0*cr

cos.alt=cos(alt.rad)
sin.alt=sin(alt.rad)

#compute view factor
vf = sum(cos.alt*sin.alt*dat.gf$Gap.Can)*dazi*dalt/pi 
  

###
### Part 3: Calculate canopy gap fraction at ----
### sun's sky position at time t
###

solzen.deg=(solzen/cr)
azi.deg=psi

ii=1+floor((90 - solzen.deg)/5)
jj=1+floor((180 - azi.deg)/5)

i=dat.gf$Alt
j=dat.gf$Azi

gt=NULL
for (k in 1:length(ii)) {gt[k]=dat.gf$Gap.Can[ii[k]==i & jj[k]==j]}

###
### Part 5: Full Q* model ----
###
###

#sigma -> Stefan Boltzman constant
sigma = 5.67*10^-8

#Dt -> direct component of incident solar radiation at time t (Wm^-2)
Dt = Kdir

#St -> diffuse component of incident solar radiation at time t (Wm^-2)
St = Kdiff

#*****************************************
#replacing NA values with zeros

Dt[is.na(Dt)] = 0
St[is.na(St)] = 0
gt[is.na(gt)] = 0

# modelled NR
NR.mod = (1-a)*((Dt)*(gt)+(St)*(vf)) + ew*((vf)*(eain)+(1-vf)*(ef))*sigma*(dat[,Ta] + 273.2)^4 - ew*sigma*(dat$Tw + 273.2)^4

# Kstar
Kstar  = (1-a)*((Dt)*(gt)+(St)*(vf))
  
#Kdir.mod for export
  
Kdir.mod = (1-a)*Dt*gt  

Kdiff.mod = (1-a)*(St)*(vf)

# Ldown absorbed
Ldown =  ew*((vf)*(eain)+(1-vf)*(ef))*sigma*(dat[,Ta] + 273.2)^4

# Lup
#Lup =  - ew*sigma*(dat$Tw + 273.2)^4                     

# Make a dataframe to add values to

  
  dfout <- data.frame(ISO = tiso, Kdir = Kdir.mod, Kdiff = Kdiff.mod, Ldown = Ldown)  
  colnames(dfout)[2] = paste0('Kdir',as.numeric(str_extract_all(h, "[0-9]+")[[1]]))
  colnames(dfout)[3] = paste0('Kdiff',as.numeric(str_extract_all(h, "[0-9]+")[[1]]))
  colnames(dfout)[4] = paste0('Ldown',as.numeric(str_extract_all(h, "[0-9]+")[[1]]))
  
dflist <- list(dfout,vf)  
  
return(dflist)
  
}  

X = Qcalcs("Data/GLA Output .txt Files/DSCN7623.txt", kdir.in = dat$kdirfin, Ta = 'ta', eain = dat$ea.atm)


sigma = 5.67*10^-8
dat$Ldownopen =  dat$ea.atm*sigma*(dat[,Ta] + 273.2)^4

dat2 <- dat %>% select(ISO,Ldownopen)
write.csv(dat2,'Output/Ldown open site.csv')

###

###
### Step 3 Loop through and create column for each ----
###

### set working directory

setwd("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/GLA Output .txt Files")

files <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/GLA Output .txt Files")

# Get the hemis photos for each reach that were organized into folders
DUT1.1 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/1.1-DUT")
LBES1.2 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/1.2-LBES")
DUT2.1 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/2.1-DUT")
LBES2.1 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/2.1-LBES")
UBES2.2 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/2.2-UBES")
LBES2.3 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/2.3-LBES")
UBES3.1 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/3.1-UBES")
DUT3.2 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/3.2-DUT")
UBES3.2 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/3.2-UBES")
UBES3.3 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/3.3-UBES")
DUT4.1 <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Sorted GLA output files/4.1-DUT")

## get the hemis photos for the mitigation scenarios for each each
warning("run all of the mitigation stuff separately. There are '# mit' flags to indicate where")

# mit 
#DUTmit <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Mitigation reaches GLA output/DUT1.1_2.1_3.1")
#LBESmit <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Mitigation reaches GLA output/LBES1.1_1.2_2.1")
#UBESmit <- list.files("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Mitigation reaches GLA output/UBES2.1_2.2")

#Combine reaches into a list (existing)
Reaches = list(DUT1.1, LBES1.2, DUT2.1, LBES2.1, UBES2.2, LBES2.3, UBES3.1, DUT3.2, UBES3.2, UBES3.3, DUT4.1)
Reaches.names = c("DUT1.1", "LBES1.2", "DUT2.1", "LBES2.1", "UBES2.2", "LBES2.3", "UBES3.1", "DUT3.2", "UBES3.2", "UBES3.3", "DUT4.1")

# mit scenarios
#Reaches.mit = list(DUTmit, LBESmit, UBESmit)
#Reaches.names.mit = c("DUTmit", "LBESmit", "UBESmit")


#X = Qcalcs("DSCN7623.txt", kdir.in = dat$kdirfin, Ta = 'air_temperature_2085_canesm45', eain = dat$ea.atm.m45)

### 


dfKdir.reaches <- data.frame()[1:nrow(dat),]
dfKdiff.reaches <- data.frame()[1:nrow(dat),]
dfL.reaches <- data.frame()[1:nrow(dat),]
dfL.reaches.m45 <- data.frame()[1:nrow(dat),]
dfL.reaches.m85 <- data.frame()[1:nrow(dat),]
vf.reaches <- as.numeric()

for (i in 1:length(Reaches)){   # Run this for both the existing reaches 
#for (i in 1:length(Reaches.mit)){   # Run this for Mit
  
files <- Reaches[[i]]  # For existing
  
#files <- Reaches.mit[[i]]  # For mitigation 


dfK <- data.frame()[1:nrow(dat),]
dfL <- data.frame()[1:nrow(dat),]
dfL.m45 <- data.frame()[1:nrow(dat),]
dfL.m85 <- data.frame()[1:nrow(dat),]
dfKdiff <- data.frame()[1:nrow(dat),]
vfout <- as.numeric()  

for (j in 1:length(files)){
  
X = Qcalcs(files[j], kdir.in = dat$kdirfin, Ta = 'ta', eain = dat$ea.atm)

Y = Qcalcs(files[j], kdir.in = dat$kdirfin, Ta = 'air_temperature_2085_canesm45', eain = dat$ea.atm.m45)
Z = Qcalcs(files[j], kdir.in = dat$kdirfin, Ta = 'air_temperature_2085_canesm85', eain = dat$ea.atm.m85)

dfK[,j] <- X[[1]][2]
dfKdiff[,j] <- X[[1]][3]
dfL[,j] <- X[[1]][4]
dfL.m45[,j] <- Y[[1]][4]
dfL.m85[,j] <- Z[[1]][4]

#dfK <- merge(dfK, X[[1]][,1:2], by='ISO', all=T)
#dfKdiff <- merge(dfKdiff, X[[1]][,c(1,3)], by='ISO', all=T)
#dfL <- merge(dfL, X[[1]][,c(1,4)], by='ISO', all=T)

vfout[j] = X[[2]]

  }

dfKdir.reaches[,i] <- rowMeans(dfK)      
dfKdiff.reaches[,i] <- rowMeans(dfKdiff)  
dfL.reaches[,i] <- rowMeans(dfL)
dfL.reaches.m45[,i] <- rowMeans(dfL.m45) 
dfL.reaches.m85[,i] <- rowMeans(dfL.m85) 
vf.reaches[i] <- mean(vfout) 

}

#Rename each of the files by reach name - not sure if this works yet. 

# Turn back on for existing conditions 
colnames(dfKdir.reaches) <- Reaches.names
colnames(dfKdiff.reaches) <- Reaches.names
colnames(dfL.reaches) <- Reaches.names
colnames(dfL.reaches.m45) <- Reaches.names 
colnames(dfL.reaches.m85) <- Reaches.names 

# For mit scenarios
#colnames(dfKdir.reaches) <- Reaches.names.mit
#colnames(dfKdiff.reaches) <- Reaches.names.mit
#colnames(dfL.reaches) <- Reaches.names.mit
#colnames(dfL.reaches.m45) <- Reaches.names.mit
#colnames(dfL.reaches.m85) <- Reaches.names.mit


### Write out all the files 
# Create another set of files to loop through too.. could put it in a function, or just leave good instructions

# Need to add ISO back on to each of the dataframes too. Export them each individually and then load them to the 
# main model script as a list. 

dfKdir.reaches.out  <- cbind(dat$ISO, dfKdir.reaches)   
dfKdiff.reaches.out   <- cbind(dat$ISO, dfKdiff.reaches)
dfL.reaches.out    <- cbind(dat$ISO, dfL.reaches)
dfL.reaches.m45.out     <- cbind(dat$ISO, dfL.reaches.m45)  
dfL.reaches.m85.out   <- cbind(dat$ISO, dfL.reaches.m85)  
vf.reaches.out <- cbind(Reaches.names, vf.reaches) # For existing
#vf.reaches.out <- cbind(Reaches.names.mit, vf.reaches) # For mitigation


# Store the ex ones for safe keeping while running the mit scenarios:
#out.existing <- list(dfKdir.reaches.out, dfKdiff.reaches.out, dfL.reaches.out, dfL.reaches.m45.out, dfL.reaches.m85.out, vf.reaches.out)


# For existing conditions
write.csv(dfKdir.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/existing radiation/dfKdir.ex.csv")
write.csv(dfKdiff.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/existing radiation/dfKdiff.ex.csv")
write.csv(dfL.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/existing radiation/dfL.ex.csv")
write.csv(dfL.reaches.m45.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/existing radiation/dfL.m45.ex.csv")
write.csv(dfL.reaches.m85.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/existing radiation/dfL.m85.ex.csv")
write.csv(vf.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/existing radiation/vf.ex.csv")


# For mit scenarios

#write.csv(dfKdir.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/mitigation radiation/dfKdir.mit.csv")
#write.csv(dfKdiff.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/mitigation radiation/dfKdiff.mit.csv")
#write.csv(dfL.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/mitigation radiation/dfL.mit.csv")
#write.csv(dfL.reaches.m45.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/mitigation radiation/dfL.m45.mit.csv")
#write.csv(dfL.reaches.m85.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/mitigation radiation/dfL.m85.mit.csv")
#write.csv(vf.reaches.out, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/mitigation radiation/vf.mit.csv")



##
##
##
##
###
# Line up the viewfactors with the transect numbers ----
### Stuff that was used to help Davina choose shade reaches 

hemvf <- read_xlsx("Y:/Projects - Active/1446 Bessette/1 Data/Processed Data/Transect data compilation.xlsx",
                     sheet = "hemis sum")

# Take the average of all the hemis photo VFs that are at a particular transect 

vfs <- data.frame(vf = vfout, photo = files)

vfs$photo2 = substr(vfs$photo,6,8)

hemvf$reps <- as.numeric(hemvf$hem2) - as.numeric(hemvf$hem1) + 1

hemvf$reps <- as.integer(hemvf$reps)

hemvf$reps[37] = 11

hemvf2 <- hemvf[rep(rownames(hemvf), hemvf$reps),]

hemvf2$photo2 = 1

hemvf2$photo2 <- as.numeric(hemvf2$hem1) + (sequence(hemvf2$reps)-1)

# write this out to excel and do it there :( , this is taking too long to sort out, not worth it.

write.csv(hemvf2, 'C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/shade transects processing.csv')
# IN excel, I created a sequence of hem1 values until I reached hem2, for each transect. 
# Then brought the script back over

hemv <- read.csv("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/shade transects processing (mod).csv")
hemv <- hemv %>% select(trans, photo2 = phot)

hemv$photo2 = as.numeric(hemv$photo2)
vfs$photo2 = as.numeric(vfs$photo2)

vf2 <- merge(hemv,vfs,by="photo2")


# Read in waypoints from Ecodat ----

wpts <- read_xlsx("C:/Users/dwest/Repositories/1446-daves_general_data_prep/Data/Waypoints from ecodat.xlsx")


wpts <- wpts %>% select(trans, Easting, Northing, Latitude, Longitude)


vfdf <- merge(vf2, wpts, by = "trans")

vf3 <- group_by(vfdf, trans) %>% summarize(meanvf = mean(vf)) 

vfdf2 <- distinct(vfdf, trans, .keep_all = T)
 
vf4 <- merge(vf3, select(vfdf2,c(trans,Easting,Northing,Latitude,Longitude)), by = "trans")

write.csv(vf4, "C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/viewfactors averaged for each transect.csv" )




###
# Combine transects with datetime and coordinates to estimate flow at each location for hydraulic modelling ----

# Bring in datetimes when velocity measurements were taken

"Y:/Projects - Active/1446 Bessette/1 Data/Processed Data/Transect data compilation.xlsx"

vel <- read_xlsx("Y:/Projects - Active/1446 Bessette/1 Data/Processed Data/Transect data compilation.xlsx", sheet = "trans and datetime")
vel$ISO <- as.POSIXct(strftime(vel$ISO, "%m/%d/%Y %H:%M", tz = "America/Los_Angeles"))

vel$ISO <- as.POSIXct(vel$ISO, tz="America/Los_Angeles", format="%m/%d/%Y %H:%M")

# Bring in flow data from Patrick L

flow <- read.csv("C:/Users/dwest/Repositories/1446_bessette_flow_temp_synth/Output/Bessette Flow synthesized V2.csv")
flow$ISO <- as.POSIXct(ymd_hms(flow$DateTime))
flow$ISO <- force_tz(flow$ISO, tzone = "America/Los_Angeles")

### merge flow data with velocities, but only keep the spots where there is a velocity measurement

vel$ISO <- as.POSIXct(round(vel$ISO, "hours"))

vel2 <- merge(vel, flow, by = "ISO")

vel3 <- merge(vel2, vfdf2, by = "trans")

### Send this back to excel to calculate the distance between gauges and interpolate the flow  ----

write.csv(vel3,"C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/flows cor to velocity mmts.csv" )


###
# Calculate vapour pressure of the air, from Leach and Moore (2010)) ----
###


# Existing

dat$esat <- 0.611*exp((17.27*dat$ta/(dat$ta + 237.26)))
dat$ea <- (dat$rh/100)*dat$esat
dat$esat = NULL

# climate change m45

dat$esatm45 <- 0.611*exp((17.27*dat$air_temperature_2085_canesm45/(dat$air_temperature_2085_canesm45 + 237.26)))
dat$ea.m45 <- (dat$rh/100)*dat$esatm45
dat$esatm45 = NULL

# climate change m85

dat$esatm85 <- 0.611*exp((17.27*dat$air_temperature_2085_canesm85/(dat$air_temperature_2085_canesm85 + 237.26)))
dat$ea.m85 <- (dat$rh/100)*dat$esatm85
dat$esatm85 = NULL


write.csv(dat,"C:/Users/dwest/Repositories/1446-daves_general_data_prep/Output/climate master.csv" )






















